class Post < ApplicationRecord
  belongs_to :user

  has_many :taggings
  has_many :tags, through: :taggings, dependent: :destroy

  validates :title, presence: true
  mount_uploader :image, ImageUploader
  # Pretvorio sam ti metodu klase u scope jer tako je možeš vezati, naprimjer:
  # Post.search('term').tagged_with('tag')...
  scope :tagged_with, ->(id){ joins(taggings: [:tag]).where(tags: { id: id }) }

  #def self.tagged_with(id)
  # #Tag.find_by_name!(name).posts
  # Tag.find_by_id!(id).posts
  #end

  def tag_list
    tags.join(", ")
  end

  def tag_list=(names)
    self.tags = names.split(", ").map do |n|
      Tag.where(name: n.strip).first_or_create!
    end
  end
end

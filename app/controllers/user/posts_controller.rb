class User::PostsController < User::BaseController
  before_action :set_post, only: %i[ show edit update destroy ]
  helper_method :sort_column, :sort_direction

  # GET /posts
  # Malo sam ti refaktorirao ovo, imat ćeš kasnije i search postova pa će ti biti preglednije/jednostavnije
  def index
    # Dodao sam includes jer na indexu imaš ispis tagova i N+1 query koji uvijek treba izbjegavati
    # Prije - https://d.pr/i/asTdxZ
    # Poslije - https://d.pr/i/E8i8vm
    @posts = current_user.posts.includes(:tags).order("#{sort_column} #{sort_direction}")
    @posts = @posts.tagged_with(params[:tag]) if params[:tag]
    @posts = @posts.page(params[:page])
  end

  # GET /user/posts/1
  def show
  end

  # GET /user/posts/new
  def new
    @post = Post.new
  end

  # GET /user/posts/1/edit
  def edit
  end

  # POST /user/posts
  def create
    @post = Post.new(post_params)
    @post.user = current_user

    if @post.save
      redirect_to [:user, @post], notice: "Post was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /user/posts/1
  def update
    if @post.update(post_params)
      redirect_to [:user, @post], notice: "Post was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /user/posts/1
  def destroy
    @post.destroy

    redirect_to user_posts_url, notice: "Post was successfully destroyed."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = current_user.posts.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      # Dodao sam ti :image_cache parametar tu i u formu. Primjetiti ćeš da ako propadne forma zbog validacije a učitao si sliku da je
      # moraš nanovo učitati, :image_cache ti je "pamti". Da ti se field zove file, cache atribut bi bio file_cache
      params.require(:post).permit(:title, :text, :image, :image_cache, :tag_list)
    end

    def sort_column
      Post.column_names.include?(params[:sort]) ? params[:sort] : "id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
end

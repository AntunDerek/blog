class PostsController < ApplicationController
  before_action :set_post, only: :show
  helper_method :sort_column, :sort_direction

  # GET /posts
  # Malo sam ti refaktorirao ovo, imat ćeš kasnije i search postova pa će ti biti preglednije/jednostavnije
  def index
    # Dodao sam includes jer na indexu imaš ispis tagova i N+1 query koji uvijek treba izbjegavati
    # Prije - https://d.pr/i/asTdxZ
    # Poslije - https://d.pr/i/E8i8vm
    @posts = Post.includes(:tags, :user).order("#{sort_column} #{sort_direction}")
    @posts = @posts.tagged_with(params[:tag]) if params[:tag]
    @posts = @posts.page(params[:page])
  end

  # GET /posts/1
  def show
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end

    def sort_column
      Post.column_names.include?(params[:sort]) ? params[:sort] : "id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
end

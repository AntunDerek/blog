Rails.application.routes.draw do
  root to: 'posts#index'

  devise_for :users

  resources :posts, only: [:index, :show], format: false do
    # Ugnjezdio sam ti tags rutu da ruta bude ljepša /posts/tags a ne samo /tags
    # Nije greška samo da vidiš kako se to radi
    collection do
      get 'tags/:tag', to: 'posts#index', as: :tag
    end
  end

  namespace :user do
    resources :posts
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Dodao sam ti seed da imamo što gledati na stranicama
User.create!([
  {email: "admin@email.com", password: "password1234", password_confirmation: "password1234", first_name: "Admin", last_name: "User"},
  {email: "user@email.com", password: "password1234", password_confirmation: "password1234", first_name: "User", last_name: "User"},
  {email: "bob@email.com", password: "password1234", password_confirmation: "password1234", first_name: "Bob", last_name: "User"}
])

50.times do |i|
  Post.create(
    title: "Post #{i}",
    text: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>",
    tag_list: (1..10).to_a.sample(3).map{ |n| "Tag #{n}" }.join(', '),
    remote_image_url: "https://loremflickr.com/800/600",
    created_at: DateTime.current - rand(1..20).days,
    user_id: rand(1..3)
  )
end